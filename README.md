# Audiobooks

```bash
yarn install
yarn run start # will run `polymer serve ...`. That will also require npm package `polymer-cli` installed
# or
yarn run serve # will run a docker container that does `polymer serve ...`. That will only require an installed and running docker instance.
```

