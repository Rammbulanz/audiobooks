const gulp = require("gulp");
const sass = require("gulp-sass");
const tsconfig = require("./tsconfig.json");
const litStyle = require("./scripts/gulp/lit-style");

// Building scss files

gulp.task("build:sass", () => gulp.src('./src/**/*.scss')
    .pipe(sass.sync().on('error', sass.logError))
    .pipe(litStyle())
    .pipe(gulp.dest(tsconfig.compilerOptions.outDir)));


// Watching tasks

gulp.task("watch", function () {
    gulp.watch('./src/**/*.scss', ["build:sass"])
});