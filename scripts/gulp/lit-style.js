/// <reference types="@types/node" />

const through2 = require("through2");
const path = require("path");

/**
 * 
 * @param {File} file 
 * @param {string} enc 
 * @param {Function} cb 
 */
function litStyle(file, enc, cb) {
    const code = file.contents.toString(enc);

    file.contents = Buffer.from(`
        import { html } from 'lit-html';
        export default html\`
            <style>
                ${code}
            </style>
        \`;
    `);

    file.history.push(
        file.history[0] + ".js"
    );

    // console.log(file.history);

    return cb(null, file);
}

module.exports = () => through2.obj(litStyle);