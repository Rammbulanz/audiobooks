import { VaadinElement } from "./vaadin-element";

export interface VaadinUploadElement extends VaadinElement {
	files?: File[]
	accept?: string
	capture: string
	formDataName?: string
	headers?: Record<string, string>
	maxFiles?: number
	maxFileSize?: number
	maxFilesReached?: boolean
	noAuto?: boolean
	nodrop?: boolean
	target?: string
	timeout?: number
	withCredentials?: boolean
	uploadFiles(files?: Array<File>): void
}