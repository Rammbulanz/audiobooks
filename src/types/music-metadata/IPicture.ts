/**
 * Attached picture, typically used for cover art
 */
export interface IPicture {
    /**
     * Image mime type
     */
    format: string;
    /**
     * Image data
     */
    data: Uint8Array;
    /**
     * Optional description
     */
    description?: string;
    /**
     * Picture type
     */
    type?: string;
}