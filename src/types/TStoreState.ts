import { Book } from "../common/Book";
import { View } from "../enums/View";
import { Database } from "../common/database";

export type TStoreState = {
   library: Book[]
	selectedBook: Book
	playingBook: Book
	view: View
	database: Database
}