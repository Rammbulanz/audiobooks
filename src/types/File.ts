export interface File {
   bookId: string
   url: string
   name: string
   duration: number
}