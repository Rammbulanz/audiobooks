export function triggerOnce<TEvent = Event>(element: Element, type: string, callback: (e: TEvent) => void) {

   const f = (e) => {      
      element.removeEventListener(type, f);
      callback(e);
   }

   element.addEventListener(type, f);

}