import { ICommonTagsResult } from '../types/music-metadata/ICommonTagsResult'
import { parseFile } from './audio-metadata-parsers';

export type Book = {
	id: string
	title: string
	franchise?: string
	author: string
	reader: string
	/** List of URLs */
	files: FileInfo[]
	/** Index to the file in files array */
	currentFile: number
	/** Last saved timestamp on the current file */
	currentFileTime?: number
	picture?: Blob
};

export type FileInfo = {
	url: string
	file?: File
	metadata?: ICommonTagsResult
}


/**
 * Check, if a book is completely offline available
 */
export async function book_isOfflineAvailable(book: Book) {

	try {

		const { store } = await import('../store');

		const database = store.getState().database;
		const availableKeys = await database.getAll("files");

		for (const requiredKey of book.files) {
			if (availableKeys.indexOf(requiredKey.url) < 0) {
				return false;
			}
		}

		return true;

	} catch (err) {
		return false;
	}

}

/**
 * 
 */
export async function book_getCurrentFile(book: Book): Promise<File> {

	const entry = book.files[book.currentFile];


	if (!entry.file) {

		let updated = false;

		const { store } = await import('../store');
		const state = store.getState();

		const file_info = await state.database.get<FileInfo>("files", entry.url);

		// if (!file_info.metadata) {
		// 	file_info.metadata = await parseMetadata(file_info.file);
		// 	updated = true;
		// }

		if (file_info) {
			entry.file = file_info.file;
			entry.metadata = file_info.metadata;
		}

		if (updated) {
			await state.database.put("files", file_info);
		}

	}

	return entry.file;
}

export async function requestMetadata(file: File): Promise<ICommonTagsResult> {

	const res = await fetch("api/audio/metadata", {
		method: "POST",
		headers: {
			"Content-Type": file.type,
			"Content-Length": String(file.size)
		},
		body: file
	});

	if (res.status == 200) {
		return await res.json() as ICommonTagsResult;
	} else {
		console.error(`${res.url} - Status: ${res.status}, ${res.statusText}`);
		console.error(await res.text());
	}

}

export async function parseMetadata(file: File): Promise<any> {
	return await parseFile(file);
}

/**
 * Get a book from the database using its id
 */
export async function getBookById(id: string) {

	const { store } = await import('../store');
	const state = await store.getState();

	return state.database.get<Book>("library", id);

}