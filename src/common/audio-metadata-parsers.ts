import * as comlink from 'comlinkjs'

export type AudioMetadata = {
	title?: string
	artist?: string
	album?: string
	speed?: number
	genre?: string
	startTime?: string
	endTime?: string
}

const worker = new Worker("app/common/audio-metadata-parsers-worker.js");
const link = comlink.proxy(worker);


export function parseFile(file: File): Promise<any> {
	return link.parseFile(file);	
}