import { Book } from "./Book";

const DEBUG_BOOKS: Book[] = [
];

function upgradeCallback(db: IDBDatabase) {

	if (!db.objectStoreNames.contains("library")) {

		const store = db.createObjectStore("library", {
			keyPath: "id"
		});

		for (const book of DEBUG_BOOKS) {
			store.add(book);
		}

	}

	if (!db.objectStoreNames.contains("files")) {

		db.createObjectStore("files", {
			keyPath: "url"
		});

	}

}

export const initialize = open;
export const getDatabase = open;

export class Database {

	private _name: string
	private _connect: Promise<IDBDatabase>;

	constructor(name: string) {
		this._name = name;
	}

	public connect(): Promise<IDBDatabase> {

		if (!this._connect) {

			this._connect = new Promise<IDBDatabase>((fulfill, reject) => {

				const request = window.indexedDB.open(this._name, 1);

				request.onerror = () => reject(request.error);
				request.onsuccess = () => {
					let database = request.result;
					fulfill(database)
				};

				request.onupgradeneeded = () => upgradeCallback(request.result);

			});

		}

		return this._connect;
	}
	public async createTransaction(storeNames: string[], mode: IDBTransactionMode = "readonly"): Promise<IDBTransaction> {
		const db = await this.connect();
		return db.transaction(storeNames, mode);
	}
	private awaitRequest<TResult>(request: IDBRequest): Promise<TResult> {
		return new Promise((fulfill, reject) => {
			request.onerror = () => reject(request.error);
			request.onsuccess = () => fulfill(request.result);
		});
	}

	public async add(storeName: string, ...data: any[]): Promise<void> {
		const transaction = await this.createTransaction([storeName], "readwrite");
		const store = transaction.objectStore(storeName);

		const promises = data.map(value => {
			const request = store.add(value);
			return this.awaitRequest<IDBValidKey>(request);
		});

		await promises;
	}
	public async put(storeName: string, ...data: any[]): Promise<void> {
		const transaction = await this.createTransaction([storeName], "readwrite");
		const store = transaction.objectStore(storeName);

		const promises = data.map(value => {
			const request = store.put(value);
			return this.awaitRequest<void>(request);
		});

		await promises;
	}

	public async get<TResult>(storeName: string, key: string) {
		const transaction = await this.createTransaction([storeName], "readonly");
		const store = transaction.objectStore(storeName);

		const request = store.get(key);
		return await this.awaitRequest<TResult>(request);
	}
	public async getAll<TResult>(storeName: string) {
		const transaction = await this.createTransaction([storeName], "readonly");
		const store = transaction.objectStore(storeName);

		const request = store.getAll();
		return await this.awaitRequest<TResult[]>(request);
	}

	public async delete(storeName: string, key: string) {
		const transaction = await this.createTransaction([storeName], "readwrite");
		const store = transaction.objectStore(storeName);

		const request = store.delete(key);
		return await this.awaitRequest<void>(request);
	}

}