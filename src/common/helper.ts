
// ObjectUrls: Remember blobs url, 
// so you will have one url for one blob.
// ================================================

const _blobUrlMap = new Map<Blob, string>();

export function getBlobUrl(blob: Blob) {
    
    if (!_blobUrlMap.has(blob)) {
        _blobUrlMap.set(blob, URL.createObjectURL(blob));
    }

    return _blobUrlMap.get(blob);
}

export function revokeBlobUrl(blob: Blob) {
    if (_blobUrlMap.has(blob)) {
        const url = _blobUrlMap.get(blob);
        URL.revokeObjectURL(url);
        _blobUrlMap.delete(blob);
    }
}