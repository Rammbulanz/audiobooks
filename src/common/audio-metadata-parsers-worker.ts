import { Exposable, Endpoint } from 'comlinkjs'

// importScripts("https://cdn.jsdelivr.net/npm/comlinkjs@3.1.1/umd/comlink.js")
importScripts(
	location.origin + "node_modules/comlinkjs/umd/comlink.js",
	location.origin + "node_modules/"
)

declare namespace Comlink {
	export function expose(rootObj: Exposable, endpoint: Endpoint | Window): void
}
declare function id3(input: File, callback: (err: Error, tags: Record<string, any>) => void): void;

// http://id3.org/id3v2.4.0-structure

namespace AudioMetadataParser {

	export function parseFile(file: File) {

		// const reader = new FileReader();

		// reader.onload = (ev: ProgressEvent) => {
		// 	parseMp3(reader.result as ArrayBuffer);
		// };

		// reader.readAsArrayBuffer(file);

		return new Promise(function (fulfill, reject) {
			switch (file.type) {
				case "audio/mp3":
					id3(file, (err, tags) => {
						if (err) {
							
						}
						console.log(tags);
						fulfill(tags);
					});
					break;
			}
		});

	}

	export function parseMp3(buffer: ArrayBuffer) {

	}

	export function parse_ID3(buffer: ArrayBuffer) {

		let pointer = 0;

		const header = new Uint8Array(buffer.slice(pointer, 3));
		console.log("Tag", String.fromCharCode(...header));

		pointer += 10;

		const buffer_title = new Uint8Array(buffer.slice(pointer * 8, 60 * 8));
		console.log("Title", String.fromCharCode(...buffer_title));

		pointer += 60;

	}

}

Comlink.expose(AudioMetadataParser, self);