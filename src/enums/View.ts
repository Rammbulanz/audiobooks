export enum View {
   Library = "library",
   CurrentBook = "currentBook",
   Browse = "browse",
   Upload = "upload"
}