import { LitElement, html, customElement, property, query } from '@polymer/lit-element';
import { TStoreState } from "../types/TStoreState";
import { store } from "../store";
import { connect } from "pwa-helpers/connect-mixin";
import { Book, book_getCurrentFile } from "../common/book";
import styles from './panel-current-playback.scss'

function secondsToTimeString(input: any): string {

    var sec_num = parseInt(input, 10); // don't forget the second param
    var hours: number = Math.floor(sec_num / 3600);
    var minutes: number = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds: number = sec_num - (hours * 3600) - (minutes * 60);

    let result = "";

    if (hours > 0) {
        result += hours + ":";
    }

    result += (minutes < 10 ? "0" : "") + minutes + ":";
    result += (seconds < 10 ? "0" : "") + seconds + ":";

    return result;

}

@customElement("panel-current-playback" as any)
export class PanelCurrentPlaybackElement extends connect(store)(LitElement) {

    @property({ type: Object })
    private book: Book;
    @property({ type: Boolean, reflect: true })
    public playing: boolean;
    @property({ type: Boolean })
    public shuffle: boolean;
    @property({ type: Boolean })
    public repeat: boolean;

    private _currentUrl: string;
    private _audio: HTMLAudioElement;

    @query("#current-time")
    private readonly currentTimeOutput: HTMLDivElement;
    @query("#progress")
    private readonly progressBar: any;

    private get audio(): HTMLAudioElement {
        return this._audio;
    }

    public connectedCallback() {

        const audio = this._audio = document.getElementById("player") as HTMLAudioElement;

        // Attach Events
        audio.addEventListener("play", (e) => this.onAudioPlay(e));
        audio.addEventListener("timeupdate", (e) => this.onAudioTimeUpdate(e));
        audio.addEventListener("pause", (e) => this.onAudioPause(e));
        audio.addEventListener("ended", (e) => this.onAudioEnded(e));
        audio.addEventListener("loadedmetadata", (e) => this.onAudioMetadataLoaded(e));
        audio.addEventListener("durationchange", (e) => this.onAudioDurationChange(e));

        super.connectedCallback();
    }

    protected render() {
        if (!this.book) {
            return html``;
        }

        return html`
            ${styles}
            
            <div id="lower" class="">
                <div id="title-info">

                </div>
                <div id="main-controls">
                    <div class="buttons">
                        <paper-icon-button icon="vaadin:random"></paper-icon-button>
                        <paper-icon-button icon="vaadin:step-backward"></paper-icon-button>
                        ${this.playing
                ? html`<paper-icon-button icon="vaadin:pause" @click="${() => this.audio.pause()}"></paper-icon-button>`
                : html`<paper-icon-button icon="vaadin:play-circle-o"  @click="${() => this.audio.play()}"></paper-icon-button>`
            }                       
                        <paper-icon-button icon="vaadin:step-forward"></paper-icon-button>
                        <paper-icon-button icon="vaadin:refresh"></paper-icon-button>
                    </div>
                    <div class="flex">
                        <div id="current-time"></div>
                        <vaadin-progress-bar id="progress" class="flex-grow"></vaadin-progress-bar>
                        <div id="total-time">4:00</div>
                    </div>
                </div>
                <div id="side-controls">

                </div>
            </div>
                        
        `;
    }

    private onAudioPlay(e: any) {
        this.playing = true;
    }
    private onAudioPause(e: any) {
        this.playing = false;
    }
    private onAudioTimeUpdate(e: any) {
        this.currentTimeOutput.textContent = secondsToTimeString(this.audio.currentTime);
        this.progressBar.value = this.audio.currentTime;
    }
    private async onAudioEnded(e: any) {

        const book = this.book;

        if (book.currentFile < this.book.files.length) {
            book.currentFile++;
            book.currentFileTime = 0;
            await this.updateAudioSource();
        }

    }
    private onAudioDurationChange(e: any) {
        console.log("Event: durationchange");
    }
    private onAudioMetadataLoaded(e: any) {

    }

    private async updateAudioSource() {
        if (this.book) {
            if (this.book.currentFile < this.book.files.length) {
                const file = await book_getCurrentFile(this.book);
                const url = URL.createObjectURL(file);
                this.audio.src = url;
                this.audio.play();
            }
        }
    }

    public stateChanged(state: TStoreState) {
        if (this.book !== state.playingBook) {
            this.book = state.playingBook;
            this.updateAudioSource();
        }
    }

}