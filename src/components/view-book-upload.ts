import { html } from "lit-html";
import { customElement, property, query, LitElement } from "@polymer/lit-element";
import { Book, FileInfo, requestMetadata } from "../common/Book";
import { store } from "../store";
import { SWITCH_VIEW } from "../actions/view";
import { View } from "../enums/View";
import { FilesZoneElement } from "./elements/files-zone";
import {AnyAction} from "redux";
import styles from './view-book-upload.scss';

function uuid(): string {
	function s4() {
		return Math.floor((1 + Math.random()) * 0x10000)
			.toString(16)
			.substring(1);
	}
	return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
}

@customElement("view-book-upload" as any)
export class ViewBookUploadElement extends LitElement {

	@property({ type: String })
	public title: string
	@property({ type: String })
	public author: string

	@property({ type: Object })
	public picture: File = null

	@query('files-zone')
	public filesDropZone: FilesZoneElement;

	protected render() {
		return html`
			${styles}
			
			<div id="form">
			
				<vaadin-text-field id="title-input" label="Title" @change="${(e: any) => this.title = e.target.value}"></vaadin-text-field>
				<vaadin-text-field id="author-input" label="Author" @change="${(e: any) => this.author = e.target.value}"></vaadin-text-field>
			
				<files-zone accept="audio/*"></files-zone>
				<vaadin-button id="btnSubmit" theme="primary" @click="${this._submit}">Add</vaadin-button>
			
			</div>
			
		`;
	}

	private async _submit() {

		if (!this.title) {
			return alert("No title.");
		}
		if (!this.author) {
			return alert("No author");
		}
		if (this.filesDropZone.files.length == 0) {
			return alert("No files.");
		}

		// Prepare files

		const bookFiles: FileInfo[] = this.filesDropZone.files.map(file => {
			return {
				url: `/uploaded/${this.title}/${file.name}`,
				file
			} as FileInfo;
		});

		const metadataTasks = bookFiles.map(async fileInfo => {
			fileInfo.metadata = await requestMetadata(fileInfo.file);
		});
		await Promise.all(metadataTasks);

		// Build book

		const book: Book = {
			id: uuid(),
			title: this.title,
			author: this.author,
			reader: "",
			files: bookFiles.map(f => {
				return { url: f.url };
			}),
			picture: this.picture,
			currentFile: 0
		};

		// Put to database

		const database = store.getState().database;
		await database.put("library", book);
		await database.put("files", ...bookFiles);

		store.dispatch({
			type: SWITCH_VIEW,
			view: View.Library
		} as AnyAction);

	}

}