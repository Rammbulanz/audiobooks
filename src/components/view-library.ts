import { customElement, html, property, LitElement } from "@polymer/lit-element";
import { connect } from 'pwa-helpers/connect-mixin'
import { store } from '../store';
import { Book } from "../common/Book";
import { TStoreState } from "../types/TStoreState";
import { SELECT_BOOK } from "../actions/library";
import styles from './view-library.scss';

@customElement("view-library" as any)
export class ViewLibraryElement extends connect<TStoreState>(store)(LitElement) {

	@property({ type: Array })
	public library: Book[] = [];

	protected render() {
		return html`
			${styles}

			<vaadin-list-box>
				${this.library.map(book => html`
					<vaadin-item class="book" @click="${() => this.selectBook(book)}">
						<strong>${book.title}</strong>
						<vaadin-progress-bar value="${book.currentFile}" max="${book.files.length}"></vaadin-progress-bar>
						<div class="book-file-progress-text">File ${book.currentFile + 1} of ${book.files.length}</div>
					</vaadin-item>
				`)}
			</vaadin-list-box>
      `;
	}

	private selectBook(book: Book) {
		store.dispatch({
			type: SELECT_BOOK,
			book: book
		});
	}

	stateChanged(state: TStoreState) {
		if (state) {
			this.library = state.library;
		}
	}

}