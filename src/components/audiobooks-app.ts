import { html, customElement, property, LitElement } from '@polymer/lit-element'
import { View } from '../enums/View';
import { connect } from 'pwa-helpers/connect-mixin';
import { store } from '../store';
import { TStoreState } from '../types/TStoreState';
import { SWITCH_VIEW } from '../actions/view';
import styles from './audiobook-app.scss'

import "./panel-current-playback"

@customElement("audiobooks-app" as any)
export class AudiobooksApp extends connect(store)(LitElement) {

	@property({ type: String })
	public view: View = View.Library;

	protected render() {
		return html`
			${styles}
			<vaadin-app-layout>

				<div slot="branding">
					Audiobooks
				</div>
				
				<vaadin-tabs slot="menu">
					<vaadin-tab @click="${() => this._setView(View.Library)}">
						<iron-icon icon="vaadin:book"></iron-icon>
						My books
					</vaadin-tab>
					<vaadin-tab @click="${() => this._setView(View.Upload)}">
						<iron-icon icon="vaadin:plus"></iron-icon>
						Add a book
					</vaadin-tab>
				</vaadin-tabs>

				<div class="content">
					<view-book-upload class="page" ?active="${this.view == View.Upload}"></view-book-upload>
					<view-library class="page" ?active="${this.view == View.Library}"></view-library>			
				</div>
					
				<panel-current-playback></panel-current-playback>		
					
			</vaadin-app-layout>
      `;
	}

	private _setView(view: View) {
		store.dispatch({
			type: SWITCH_VIEW,
			view
		});
	}

	public stateChanged(state: TStoreState) {
		if (state.view !== this.view) {
			this.view = state.view;
		}
	}
}