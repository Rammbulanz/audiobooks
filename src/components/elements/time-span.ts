import { html } from "lit-html";
import { property, customElement, LitElement } from "@polymer/lit-element";

@customElement("time-span" as any)
export class TimeSpanElement extends LitElement {

    @property({ type: Number })
    public timespan: number = 0

    protected render() {

        if (Number.isNaN(this.timespan)) {
            return html`<span>NaN</span>`;
        }

        if (this.timespan < 60 * 60) {
            // use format minutes:seconds

            const minutes = Math.floor(this.timespan / 60);
            const seconds = this.timespan % minutes;

            return html`<span>${minutes}:${seconds}</span>`;

        } else {

            // use format hours:minutes:seconds

            const hours = Math.floor(this.timespan / (60 * 60));
            const minutes = Math.floor((this.timespan % hours) / 60);
            const seconds = this.timespan % minutes;

            return html`<span>${hours}:${minutes}:${seconds}</span>`;

        }
    }

}


declare global {
    interface HTMLElementTagNameMap {
        "time-span": TimeSpanElement;
    }
}