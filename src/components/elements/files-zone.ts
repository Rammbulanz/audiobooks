import { LitElement, property, customElement } from "@polymer/lit-element";
import { TemplateResult, html } from "lit-html";
import { parseMetadata } from "../../common/Book";
import styles from './files-zone.scss';

@customElement('files-zone' as any)
export class FilesZoneElement extends LitElement {

	@property({ type: Number })
	public filesLimit: number;

	@property({ type: Number })
	public fileSizeLimit: number;

	@property({ type: Array })
	public files: File[] = [];

	@property({ type: String })
	public accept: string;

	@property({ type: Boolean, reflect: true })
	protected dropAvailable: boolean

	protected render(): TemplateResult {
		return html`
			${styles}

			<div id="panel" draggable="false" @dragover="${this.onDragOver}" @drop="${this.onDrop}">

				<div>
					<vaadin-button>Add Files...</vaadin-button>
					<span>Drop files here</span>
				</div>

				<vaadin-list-box id="files">
					${this.files.map(file => html`
						<vaadin-item>
							<div class="file">
								<div class="file-info">
									${file.name}
								</div>
								<iron-icon class="file-command" icon="vaadin:vaadin-h" @click="${() => parseMetadata(file)}"></iron-icon>
								<iron-icon class="file-command" icon="vaadin:close-small" @click="${() => this.files = this.files.filter(f => f !== file)}"></iron-icon>
							</div>
						</vaadin-item>
					`)}
				</vaadin-list-box>

			</div>

		`;
	}

	protected matchTypes(left: string, right: string): boolean {

		const leftParts = left.split('/');
		const rightParts = right.split('/');

		if (leftParts.length !== rightParts.length) {
			return false;
		}

		for (let i = 0; i < leftParts.length; i++) {
			if (leftParts[i] === '*' || rightParts[i] === '*') {
				continue;
			}
			if (leftParts[i] !== rightParts[i]) {
				return false;
			}
		}

		return true;
	}

	private onDragOver(e: DragEvent) {
		e.preventDefault();

		// Set the dropEffect to move
		e.dataTransfer.dropEffect = "pointer";

	}
	private onDrop(e: DragEvent) {
		e.preventDefault();

		const files = this.files;

		for (let transferItem of e.dataTransfer.items) {

			if (this.accept && !this.matchTypes(this.accept, transferItem.type)) {
				continue;
			}

			if (transferItem instanceof DataTransferItem) {

				if (transferItem.kind !== "file") {
					continue;
				}

				const file = transferItem.getAsFile();
				files.push(file);

			}

		}

		// Sort files array
		this.files = files.sort((a, b) => {
			return ('' + a.name).localeCompare(b.name);
		});

		this.requestUpdate();
	}

}


declare global {
	interface HTMLElementTagNameMap {
		'files-zone': FilesZoneElement
	}
}