import { html } from "lit-html";
import { customElement, LitElement } from "@polymer/lit-element";
import styles from './view-browse.scss';

/**
 * This element is not implemented yet.
 * It is mentioned to let the user search for books and add them to his library.
 */
@customElement("view-browse" as any)
export class ViewBrowseElement extends LitElement {
   
   protected render() {
      return html`
         ${styles}

         <p>Not implemented!</p>
      `;
   }

}