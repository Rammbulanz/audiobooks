import { TemplateResult, html } from "lit-html";

export const lit_if = (condition: boolean, trueCallback: () => TemplateResult) => {
   if (condition)
      return trueCallback();
   return html``;
}