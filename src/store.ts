import { createStore, combineReducers, Store } from 'redux'
import { reducer as library } from './reducers/library'
import { reducer as selectedBook } from './reducers/selected-book'
import { reducer as playingBook } from './reducers/playing-book'
import { reducer as view } from './reducers/view'
import { reducer as database } from './reducers/database'
import { LIBRARY_LOAD } from './actions/library';
import { TStoreState } from "./types/TStoreState";
import { parseFile } from './common/audio-metadata-parsers';

export const store: Store<TStoreState> = createStore(combineReducers({
	library,
	view,
	selectedBook,
	playingBook,
	database
}));



store.getState().database.connect().then(database => {

	const transaction = database.transaction("library", "readonly");
	const library = transaction.objectStore("library");

	const libraryRequest = library.getAll();
	libraryRequest.onerror = () => console.log("Failed loading library.", libraryRequest.error);
	libraryRequest.onsuccess = () => {
		store.dispatch({
			type: LIBRARY_LOAD,
			library: libraryRequest.result
		});
	}

	

});

// store.getState().database.get('files', "/uploaded/Grabmahl des Sargeras/Teil 1 - Das Schicksal eines anderen.mp3").then((info:any) => {
// 	parseFile(info.file);
// });