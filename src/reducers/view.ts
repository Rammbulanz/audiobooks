import { View } from "../enums/View";
import { AnyAction } from "redux";
import { SWITCH_VIEW } from "../actions/view";

export function reducer (state: View = View.Library, action: AnyAction): View {
   switch (action.type) {
      case SWITCH_VIEW:
			return action.view;
      default:
         return state;
   }
}