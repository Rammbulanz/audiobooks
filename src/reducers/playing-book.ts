import { Book } from "../common/Book";
import { AnyAction } from "redux";
import { SELECT_BOOK } from "../actions/library";

export function reducer(state: Book = null, action: AnyAction): Book {
	switch (action.type) {
		case SELECT_BOOK:
			return action.book;
		default:
			return state;
	}
}