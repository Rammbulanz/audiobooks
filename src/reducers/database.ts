import { Database } from "../common/database";
import { AnyAction } from "redux";

export const reducer = function (state: Database, action: AnyAction): Database {
	if (typeof state === "undefined") {
		state = new Database("audiobooks");
	}
	return state;
}