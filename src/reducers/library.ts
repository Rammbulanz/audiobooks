import { Book } from "../common/Book";
import { AnyAction } from 'redux'
import { LIBRARY_LOAD } from "../actions/library";

export function reducer(state: Book[] = [], action: AnyAction): Book[] {
	switch (action.type) {
		case LIBRARY_LOAD:
			return action.library;
		default:
			return state;
	}
}